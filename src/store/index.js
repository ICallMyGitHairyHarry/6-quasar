import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

// example
export default new Vuex.Store({
  state: {
    commentsList: [{
      name: 'Wendy Simmmons',
      date: '2018-10-21',
      text: 'Anim sint aliquip nostrud qui Lorem quis amet sint laborum laborum. Sit cillum elit officia adipisicing labore est ad incididunt dolore amet voluptate occaecat nulla. Dolore deserunt ex ipsum duis id dolore duis aute incididunt nostrud commodo esse dolore adipisicing. Ut pariatur ex commodo ex labore ad eiusmod id voluptate reprehenderit commodo veniam. Tempor elit ex velit anim nulla. Nisi nulla consectetur ipsum proident deserunt adipisicing consectetur cillum anim. Id dolore Lorem ullamco dolore velit duis esse culpa veniam eiusmod consequat magna sint irure. Occaecat id ut officia magna dolore. Magna enim sit Lorem ullamco occaecat. Non culpa occaecat quis velit ipsum officia consequat officia do sit adipisicing exercitation aliqua. Laboris ipsum cupidatat fugiat nulla aute Lorem amet est ex anim proident. Cupidatat reprehenderit tempor ullamco aliquip laboris sunt id aute Lorem ex sint ullamco fugiat. Ipsum ad voluptate sit sit est incididunt nostrud minim sint aliquip in deserunt minim culpa. Laborum esse do ut id voluptate nostrud sunt. Fugiat excepteur quis voluptate incididunt reprehenderit ea. Commodo nisi eu velit velit est duis cillum. Nostrud magna ad tempor ad reprehenderit. Reprehenderit proident nostrud esse mollit cillum laborum velit anim laborum aliqua in est tempor velit. Sunt dolore pariatur ex ut ullamco exercitation nisi esse laboris ex laborum voluptate commodo dolor. Amet adipisicing dolor exercitation exercitation cillum adipisicing velit id. Sit amet laborum quis labore laborum irure. Eu eu do velit cillum anim non non sint. Ex labore aliqua sunt mollit nostrud aliquip nostrud duis occaecat. Tempor commodo reprehenderit exercitation enim Lorem aliqua nisi non amet fugiat sit aliqua in. Aliqua officia ut incididunt cupidatat ut. Occaecat veniam deserunt dolor duis Lorem amet eiusmod pariatur. Ut et nulla aliquip sit anim irure mollit commodo officia pariatur mollit aliqua do. Ut eiusmod ipsum id commodo.'
    },
    {
      name: 'Jane Jones',
      date: '2019-09-21',
      text: 'Anim sint aliquip nostrud qui Lorem quis amet sint laborum laborum. Sit cillum elit officia adipisicing labore est ad incididunt dolore amet voluptate occaecat nulla. Dolore deserunt ex ipsum duis id dolore duis aute incididunt nostrud commodo esse dolore adipisicing. Ut pariatur ex commodo ex labore ad eiusmod id voluptate reprehenderit commodo veniam. Tempor elit ex velit anim nulla. Nisi nulla consectetur ipsum proident deserunt adipisicing consectetur cillum anim. Id dolore Lorem ullamco dolore velit duis esse culpa veniam eiusmod consequat magna sint irure. Occaecat id ut officia magna dolore. Magna enim sit Lorem ullamco occaecat. Non culpa occaecat quis velit ipsum officia consequat officia do sit adipisicing exercitation aliqua. Laboris ipsum cupidatat fugiat nulla aute Lorem amet est ex anim proident. Cupidatat reprehenderit tempor ullamco aliquip laboris sunt id aute Lorem ex sint ullamco fugiat. Ipsum ad voluptate sit sit est incididunt nostrud minim sint aliquip in deserunt minim culpa. Laborum esse do ut id voluptate nostrud sunt. Fugiat excepteur quis voluptate incididunt reprehenderit ea. Commodo nisi eu velit velit est duis cillum. Nostrud magna ad tempor ad reprehenderit. Reprehenderit proident nostrud esse mollit cillum laborum velit anim laborum aliqua in est tempor velit. Sunt dolore pariatur ex ut ullamco exercitation nisi esse laboris ex laborum voluptate commodo dolor. Amet adipisicing dolor exercitation exercitation cillum adipisicing velit id. Sit amet laborum quis labore laborum irure. Eu eu do velit cillum anim non non sint. Ex labore aliqua sunt mollit nostrud aliquip nostrud duis occaecat. Tempor commodo reprehenderit exercitation enim Lorem aliqua nisi non amet fugiat sit aliqua in. Aliqua officia ut incididunt cupidatat ut. Occaecat veniam deserunt dolor duis Lorem amet eiusmod pariatur. Ut et nulla aliquip sit anim irure mollit commodo officia pariatur mollit aliqua do. Ut eiusmod ipsum id commodo.'
    }],
    allCategories: [
      {
        name: '«Было так хорошо, пока не пришли феминистки»',
        author: 'Jane Simmmons',
        date: '2013-03-03'
      },
      {
        name: 'excepteur cillum aliquip eu sint veniam ipsum aute aliquip qui aliquip magna pariatur laborum non',
        author: 'Calvin Miles',
        date: '2017-10-05'
      },
      {
        name: 'pariatur ea tempor exercitation elit ullamco proident nostrud ipsum et excepteur tempor deserunt nulla quis',
        author: 'Albert Howard',
        date: '2016-04-22'
      },
      {
        name: 'anim id aliquip proident qui proident fugiat ea sit ullamco sint nostrud sunt do ad',
        author: 'Wade Simmmons',
        date: '2018-02-15'
      },
      {
        name: 'duis culpa elit est proident ad anim adipisicing laborum labore',
        author: 'Theresa Black',
        date: '2015-03-18'
      },
      {
        name: 'voluptate sunt cillum',
        author: 'Shawn Pena',
        date: '2018-02-15'
      },
      {
        name: 'velit occaecat ad duis laborum aute est',
        author: 'Savannah Henry',
        date: '2017-10-24'
      },
      {
        name: 'dolor magna culpa labore voluptate cillum fugiat eu laboris ad ea occaecat',
        author: 'Julie Mckinney',
        date: '2014-08-21'
      }
    ],
    articles1: [
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet,consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla vestibu...',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      },
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet,consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla vestibu...',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      }
    ],
    articles2: [
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla ',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      },
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla ',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      },
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla ',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      }
    ],
    articles3: [
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet,consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla vestibu...',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      },
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet,consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla vestibu...',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      }
    ],
    articles4: [
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla ',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      },
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla ',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      },
      {
        category: 'Категория',
        headline: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed turpis nec sem malesuada fringilla. Nulla ',
        author: 'Бибип Нурхамогамедов',
        time: 'сегодня 4:20'
      }
    ],
    paragraph: [
      {
        headline: 'Простой способ вытереть лобовое стекло до идеала, изнутри',
        category: 'автомобили',
        author: 'Вася Пупкин',
        text1: 'Поэтому многие откладывают данный процесс до последнего, пока на стекле не появятся жирные блики или же обзорность ночью от света встречных фар автомобилей не начнет ухудшаться. Но есть пара способов, которы очень упрощают эту процедуру и позволяют проделать процесс в кратчайшие сроки.',
        text2: 'Первый и самый распространенный это раствор из нашатырного спирта, а именно в соотношении 15 мл нашатырного спирта на 1 литр воды, как следует перемешиваем и протираем, смачиваем фибру и протираем как следует, следов не останется, а стекло будет максимально прозрачным. Но если под рукой нету качественной фибры и даже нашатырного спирта, всегда приходит 2-ой способ которым пользовались еще автолюбители из СССР.',
        text3: 'Заключается он в использовании уксуса и газет (так как в чернилах их использовался свинец) речь идет о старых газетах, то они с легкостью после протирки уксусом стирали даже самые глубокие жирные пятена от рук или дымного нагара, если в салоне курили. Этот самый незамысловатый способ хорошо позволял отчистить лобовое стекло, да и все стекла в целом. А вообще хотя бы раз в месяц авто стоит мыть изнутри, чтобы в нем застаивалось меньше грязи и влажности, особенно это актуально в зимнее время года и для тех кто много ездит.',
        text4: 'P.s а какими средствами отчищаете Вы стекла? Пишите свое мнение в комментариях, а также подписывайтесь!',
        date: '2017-06-01'
      }
    ]
  },
  mutations: {
  },
  actions: {
  },
  getters: {
  }
})
